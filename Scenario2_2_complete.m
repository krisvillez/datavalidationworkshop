%%  Scenario 2.1 - complete solution
%   This script is used to study the use of spatial redundancy for fault
%   detection. The applied methods are based on material in [1-3].
%
%   References:
%   [1] Narasimhan S, Jordache, C (2000). Data reconciliation and gross
%   error detection: An intelligent use of process data. Gulf Publishing.  
%   [2] Montgomery D C (2009). Introduction to statistical quality control.
%   John Wiley & Sons (New York).  
%   [3] Narasimhan S, Bhatt N (2015). Deconstructing principal component
%   analysis using a data reconciliation perspective. Computers & Chemical
%   Engineering, 77, 74-84.   
%% 

% Copyright (C) 2018 Kris Villez
% Copyright (C) 2018 Quan Le Hong
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

% Author:   Kris Villez <kris.villez@eawag.com>
% Created:	2018-08-22

clc
clear all
close all

if exist ("OCTAVE_VERSION", "builtin") > 0
    pkg load statistics
end

% load the data
Qtil = dlmread('data2.csv')';
Qtil = Qtil(1:4,:) ;

[J,nSample] = size(Qtil) ;

figure, hold on
    plot(Qtil','.')
    legend({'Q_{3}','Q_{4}'})

% ============================================================
%   Method 1/3: Nodal test (NT)
% ============================================================

% define the nodal balances
A = [ +1 +1 -1 0 ; 0 0 +1 -1 ];

% compute residuals
D = A*Qtil ;

% inputs
z       =	3   ;	% suitable level expressed as z-score (2-sigma)
sigma	=	1   ;	% assume measurement error standard deviation equals 1
% computation
sigma_diff1 =	sqrt(3)*sigma           ;	% standard deviation for first balance
LCL1        =	-z*sigma_diff1          ;	% compute LCL)
UCL1        =	+z*sigma_diff1          ;	% compute UCL)
sigma_diff2	=	sqrt(2)*sigma           ;	% standard deviation for second balance
LCL2        =	-z*sigma_diff2          ;	% compute LCL)
UCL2        =	+z*sigma_diff2          ;	% compute UCL)
CL          =	normcdf(z)-normcdf(-z)	;	% compute confidence level
disp(['Confidence level: ' num2str(CL*100,'%4.2f') '%'])


% make plots of data and nodal balance residuals
figure, 
subplot(3,1,1), hold on
    plot(Qtil','.')
    legend({'Q_{3}','Q_{4}'})
subplot(3,1,2), 
    hold on    
    plot(D(1,:)','b.')
    plot([0 nSample],[1 1]*LCL1 ,'b-')
    plot([0 nSample],[1 1]*UCL1 ,'b-')
subplot(3,1,3), hold on    
    plot(D(2,:)','r.')
    axis tight
    plot([0 nSample],[1 1]*LCL2 ,'r-')
    plot([0 nSample],[1 1]*UCL2 ,'r-')
    
    
% ============================================================
%   Method 2/3: Global test (GT)
% ============================================================

% compute weighted sum-of-squared-residual (WSSR)
sigma2	=   1               ; % assume variance is known and equal to 1.
Sigma2  =   sigma2*eye(J)   ; % assume scalar measurement error covariance matrix 
WSSR    =	nan(nSample,1)	;
for iSample=1:nSample
    WSSR(iSample) = D(:,iSample)'*(A*Sigma2*A')^(-1)*D(:,iSample);
end

% make plots of data, nodal balance residuals, and WSSR
figure, 
subplot(3,1,1), hold on
    plot(Qtil','.')
    legend({'Q_{3}','Q_{4}'})
subplot(3,1,2), 
    hold on    
    plot(D(1,:)','b.')
    plot([0 nSample],[1 1]*( +3*sqrt(3) ) ,'b-')
    plot([0 nSample],[1 1]*( -3*sqrt(3) ) ,'b-')
    plot(D(2,:)','r.')
    axis tight
    plot([0 nSample],[1 1]*( +3*sqrt(2) ) ,'r-')
    plot([0 nSample],[1 1]*( -3*sqrt(2) ) ,'r-')
subplot(3,1,3), hold on    
    plot(WSSR','k.')
    
% compute upper control limit
%   Sources
%   [1] Montgomery, D. C. (2009). Introduction to statistical quality control.
%   John Wiley & Sons (New York). 
%   [2] https://en.wikipedia.org/wiki/Cochran's_theorem#Distributions

% inputs
z       =	2   ;	% suitable level (2-sigma)
sigma	=	1   ;	% assume measurement error standard deviation equals 1
% computations
K       =   size(A,1)                   ;   % number of constraints
UCL     =	6*K                         ;	% compute UCL
CL      =	chi2cdf(UCL,K)              ;	% compute confidence level
disp(['Confidence level: ' num2str(CL*100,'%4.2f') '%'])
    
% add UCL to plot    
plot([0 nSample],[1 1]*UCL ,'r-')

% ============================================================
%   Method 3/3: Measurement test (MT) - not implemented
% ============================================================


% ============================================================
%   In absence of prior knowledge: via PCA
% ============================================================

cal     =   1:200	; % identify samples for calibration
pc      =   2       ; % propose a number of PCs


% PCA model calibration
Qtil_cal	=	Qtil(:,1:200)                       ;   % select data
nSampleCal	=   size(Qtil_cal,2)                    ;   % number of calibration samples
mu          =	mean(Qtil_cal,2)                    ;   % compute mean
Qc          =	Qtil_cal-repmat(mu,[1 nSampleCal])	;   % center the data
[P,S,U]     =   svd(Qc,'econ')                      ;   % decomposition via SVD 

% eigenvalues:
lambda      =	diag(S).^2                          ; 

% scree plot eigenvalues - to help decide how many PCs to keep
figure, hold on
    bar( lambda )

% define Ahat based on decomposition and proposed number of PCs:
Ahat        =	(P(:,(pc+1):end)*S((pc+1):end,(pc+1):end))'	;

% compute residuals
D           =	Ahat*(Qtil-repmat(mu,[ 1 nSample ]) )	;

% compute weighted-sum-of-squared-residuals (WSSR), a.k.a. Q-statistic
WSSR        =	sum(D.^2,1)                             ;

% make plots of data, PCA-based residuals, and WSSR
figure, 
subplot(3,1,1), hold on
    plot(Qtil','.')
    legend({'Q_{3}','Q_{4}'})
subplot(3,1,2), 
    hold on    
    for d=1:size(D,1)
        plot(D(d,:)','.')
    end
    axis tight
subplot(3,1,3), hold on    
    plot(WSSR','k.')

% compute upper control limit
%   Sources:
%   [1]	Jackson, J. E. (2005). A user's guide to principal components (Vol.
%       587). John Wiley & Sons.  
% input:
z       =   3   ;   % confidence level, expressed as x-sigma (e.g. 3-sigma) 
% 1st, 2nd, 3rd order moments 
theta1	=	sum(lambda((pc+1):end).^1) ;
theta2	=	sum(lambda((pc+1):end).^2) ;
theta3	=	sum(lambda((pc+1):end).^3) ;
% exponent    
h0      =	1-2*(theta1*theta3)/(3*theta2.^2);
% compute limit
UCL     =	theta1*(z*sqrt(2*theta2*h0.^2)/theta1+ ...
                theta2*h0*(h0-1)/theta1.^2 +1 ).^(1/h0);
% compute confidence level
CL      =	normcdf(z)	;	
disp(['Confidence level: ' num2str(CL*100,'%4.2f') '%'])
    
% plot limit
plot([0 nSample],[1 1]*UCL ,'r-')
    
return
