# datavalidationworkshop

This repository contains the Matlab/Octave files for a workshop on "Principles of Data Validation", originally held at the 2018 IWA World Water Congress in Tokyo. These files have been succesfully tested in Octave v4.4.1. and Matlab R2017b.

Instructions:
1. Install Octave version 4.4.1. Octave is available for download here: https://www.gnu.org/software/octave/download.html 
2. Place the files in this package in a directory of your liking, e.g. C:/workshop/ . This will be the workshop directory.
3. Start Octave
4. In Octave, move to the workshop directory to make it the current directory, e.g. type "cd C:/workshop/" (without quotes)
5. In Octave, try out the script "SimulateData.m", e.g. by typing "SimulateData" (without quotes)