%%  Simulate data for data validation excercises
%   This script simulates noise data sets as a basis for exercises in data
%   validation. The simulation is inspired by [1] and is the same, except
%   for the application of equal noise variances.
%
%   References:
%   [1] Narasimhan S, Shah S L (2008). Model identification and error
%       covariance matrix estimation from noisy data using PCA. Control
%       Engineering Practice, 16(1), 146-155.   
%% 

% Copyright (C) 2018 Kris Villez
% Copyright (C) 2018 Quan Le Hong
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

% Author:   Kris Villez <kris.villez@eawag.com>
% Created:	2018-08-22

clc
clear all
close all

% number of samples
nSample =1000;

% generate noise-free data:
Q(:,1)  =   42 + 23*sin((1:nSample)'/nSample*42) ;
Q(:,2)  =   73 + 11*cos((1:nSample)'/nSample*73) ;
Q(:,3)  =	Q(:,1)+Q(:,2)   ;
Q(:,4)  =	Q(:,3)          ;
Q(:,5)  =   Q(:,1)          ;

% generate noisy data:
Qtil1       =	Q(:,3)+randn(nSample,3)     ; % for scenario 1 - hardware redundancy
Qtil2       =	Q(:,1:4)+randn(nSample,4)   ; % for scenario 2.x - spatial redundancy

% add a drift fault to the third sensor in both data sets
drift       =   10*max(0,((1:nSample)-373))'/nSample; 
Qtil1(:,3)  =	Qtil1(:,3)+drift    ;
Qtil2(:,3)  =	Qtil2(:,3)+drift    ;

% make some plots of data in scenario 2
figure, hold on,
    h1 = plot(Q,'-');
    h2 = plot(Qtil2,'.');
    for i=1:length(h2)
        set(h2(i),'Color',get(h1(i),'Color'))
    end
    legend(num2str([1:5 1:4]'))
    drawnow()

% save data sets 
dlmwrite('data1.csv',Qtil1)
dlmwrite('data2.csv',Qtil2)

return
