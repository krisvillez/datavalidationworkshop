%%  Scenario 2.1 - complete solution
%   This script is used to study the use of spatial redundancy for fault
%   detection. The applied methods are based on material in [1-2].
%
%   References:
%   [1] Narasimhan S, Jordache, C (2000). Data reconciliation and gross
%   error detection: An intelligent use of process data. Gulf Publishing.  
%   [2] Montgomery D C (2009). Introduction to statistical quality control.
%   John Wiley & Sons (New York).  
%% 

% Copyright (C) 2018 Kris Villez
% Copyright (C) 2018 Quan Le Hong
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

% Author:   Kris Villez <kris.villez@eawag.com>
% Created:	2018-08-22

clc
clear all
close all

if exist ("OCTAVE_VERSION", "builtin") > 0
    pkg load statistics
end

% load the data and select measurement Q3 and Q4:
Qtil = dlmread('data2.csv')';
Qtil = Qtil(3:4,:) ;

% get meta-data: data dimensions
[J,nSample] = size(Qtil) ;

% plot the data
figure, hold on
    plot(Qtil','.')
    legend({'Q_{3}','Q_{4}'})

% compute residual
R = (Qtil(1,:)-Qtil(2,:))' ;
     
% plot the data as well as residual and LCL and UCL
figure,
subplot(2,1,1), hold on
    plot(Qtil','.')
    legend({'Q_{3}','Q_{4}'})
subplot(2,1,2), hold on    
    plot(R,'k.')
    axis tight
    
% compute sensible lower control limit (LCL) and upper control limit (UCL)
% and associated confidence level 
%   Sources
%   [1] Montgomery, D. C. (2009). Introduction to statistical quality control.
%   John Wiley & Sons (New York). 
%   [2] https://en.wikipedia.org/wiki/Cochran's_theorem#Distributions

% inputs
z       =	3   ;	% suitable level expressed as z-score (2-sigma)
sigma	=	1   ;	% assume measurement error standard deviation equals 1
% computations
sigma_diff = sqrt(2)*sigma ; % standard deviation for pair-wise difference
LCL     =	-z*sigma_diff           ;	% compute LCL)
UCL     =	+z*sigma_diff           ;	% compute UCL)
CL      =	normcdf(z)-normcdf(-z)	;	% compute confidence level
disp(['Confidence level: ' num2str(CL*100,'%4.2f') '%'])

% add computed LCL and UCL to plot    
plot([0 nSample],[1 1]*UCL ,'r-')
plot([0 nSample],[1 1]*LCL  ,'r-')

return