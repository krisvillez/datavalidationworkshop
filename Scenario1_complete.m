%%  Scenario 1 - complete solution
%   This script is used to study the use of hardware redundancy for fault
%   detection. The applied method is based on material in [1-2].
%
%   References:
%   [1] Narasimhan S, Jordache, C (2000). Data reconciliation and gross
%   error detection: An intelligent use of process data. Gulf Publishing.  
%   [2] Montgomery D C (2009). Introduction to statistical quality control.
%   John Wiley & Sons (New York).  
%% 

% Copyright (C) 2018 Kris Villez
% Copyright (C) 2018 Quan Le Hong
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program. If not, see <http://www.gnu.org/licenses/>.

% Author:   Kris Villez <kris.villez@eawag.com>
% Created:	2018-08-22

clc
clear all
close all

if exist ("OCTAVE_VERSION", "builtin") > 0
    pkg load statistics
end

% load the data
Qtil = dlmread('data1.csv')';

% make a time series plot of the data
figure, hold on
    plot(Qtil','.')
    legend({'Q_{3,1}','Q_{3,2}','Q_{3,3}'})

% get some meta-data: dimensions of data set
[J,nSample] = size(Qtil) ;

% compute mean of the redundant signals
Qhat	=	sum(Qtil,1)/J	;

% compute residuals between the measurements and the computed mean:
R       =   (Qtil-Qhat)'    ;

% compute the sum-of-squared-residuals (SSR) statistic
SSR =	sum((Qtil-Qhat).^2,1) ;

% make a time series plot of the data, the mean, the residuals, the SSR
% statistic:
figure,
subplot(3,1,1), hold on
    plot(Qtil','.')
    plot(Qhat','k.')
    legend({'Q_{3,1}','Q_{3,2}','Q_{3,3}','\mu_3'})
subplot(3,1,2), hold on
    plot(R,'.')
subplot(3,1,3), hold on
    plot(SSR,'k.')
    axis tight

% Compute upper control limit (UCL)
%   Sources:
%   [1] Montgomery, D. C. (2009). Introduction to statistical quality control.
%   John Wiley & Sons (New York). 
%   [2] https://en.wikipedia.org/wiki/Cochran's_theorem#Distributions
sigma   =	1 ; % assume the measurement error standard deviation equals 1
UCL     =   4*(J)*(sigma).^2 ; % compute a sensible choice for the limit
CL      =   chi2cdf(UCL./sigma.^2,J-1) ; % compute theoretical confidence level
disp(['Confidence level: ' num2str(CL*100,'%4.2f') '%'])

% plot the UCL 
plot([0 nSample],[1 1]*UCL ,'r-')

return